export const columnsForNewGrid = [  
  {
      field: 'title',
      title: 'Tittle',
      type: 'text'
    }, {
      field: 'url',
      title: 'Url',
      type: 'text'
    },
    {
        field: 'urlToImage',
        title: 'Image Url',
        type: 'text'
    },
    {
      field: 'description',
      title: 'Description',
      type: 'text'
  },
    {
        field: 'publishedAt',
        title: 'Published On',
        type: 'date',
        format: '{0:dd/MM/yyyy}'
    }
  ];