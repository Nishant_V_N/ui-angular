import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adddelete',
  templateUrl: './adddelete.component.html',
  styleUrls: ['./adddelete.component.css']
})
export class AdddeleteComponent implements OnInit {
  newItem: string;
  items: any;
  itemObj: any;
  constructor() {
    this.newItem = '';
    this.items= [];
   }

  ngOnInit() {
  }
  addItem(event) {
    if( this.newItem &&  this.newItem.trim().length) {
      this.itemObj = {
        newItem: this.newItem,
      }
      this.items.push(this.itemObj);
      this.newItem = '';
    }
  }
  deleteItem(event) {
    this.items.splice(event, 1);
  }
  

}
