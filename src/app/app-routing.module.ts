import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home';
import {NewsComponent} from './news';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { DragdropComponent } from './dragdrop';
import { NotfoundComponent } from './notfound';
import { AuthGuard } from './_guards';
import {AdddeleteComponent} from './adddelete';
import {ContextmenuComponent} from './contextmenu';

const routes: Routes = [
 { path: '', component: HomeComponent, canActivate: [AuthGuard] },
 { path: 'login', component: LoginComponent },
 { path: 'register', component: RegisterComponent },
//  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
 { path: 'news', component: NewsComponent, canActivate: [AuthGuard] },
 { path: 'dragdrop', component: DragdropComponent, canActivate: [AuthGuard] },
 { path: 'adddelete', component: AdddeleteComponent, canActivate: [AuthGuard] },
 { path: 'contextmenu', component: ContextmenuComponent, canActivate: [AuthGuard] },
 
  // otherwise redirect 
  { path: 'not-found', component: NotfoundComponent},
  { path: '**', redirectTo: 'not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
