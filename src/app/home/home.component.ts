import { Component, OnInit } from '@angular/core';
import { GridDataResult} from '@progress/kendo-angular-grid';
import {DataService} from '../_services';
import {pasreResponse} from '../parser/index';
import {columnsForNewGrid} from '../constants/index';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public newsData = [];
  public gridView: GridDataResult;
  public pageSize = 15;
  public skip = 0;
  public columnsForNewGrid: any[] = columnsForNewGrid;
  public loading: boolean;
  constructor(private DataService : DataService) { 
    // get news data from API 
   
    this.getNewsFromService();
  }

  ngOnInit() {
  }
  
private getNewsFromService() {
  this.loading = true;
  this.DataService.getNewsServiceCall().subscribe(response => { 
      this.newsData = pasreResponse(response); 
      console.log('news data from API ==>', this.newsData);
      this.gridView = pasreResponse(response);
      this.loading = false;
});
}
}
