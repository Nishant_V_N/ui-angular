import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
public title: string;
public url: string;
public urlToImage: string;
public publishedAt: string;
public description: string;

  constructor(route: ActivatedRoute) { 
    this.title = route.snapshot.params.title;
    this.url = route.snapshot.params.url;
    this.urlToImage = route.snapshot.params.urlToImage;
    this.description = route.snapshot.params.description;
    this.publishedAt = route.snapshot.params.publishedAt;
  }

  ngOnInit() {
  }
  

}
