import { Component, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material';

@Component({
  selector: 'app-contextmenu',
  templateUrl: './contextmenu.component.html',
  styleUrls: ['./contextmenu.component.css']
})
export class ContextmenuComponent {

  displayedColumns: string[] = ['name'];

  items = [
    { id: 1, name: 'Sachin' },
    { id: 2, name: 'MS Dhoni' },
    { id: 3, name: 'Virat Kohli' }
  ];
teamA = [];
teamB= [];
  @ViewChild(MatMenuTrigger, {static: false})
  contextMenu: MatMenuTrigger;

  contextMenuPosition = { x: '0px', y: '0px' };

  onContextMenu(event: MouseEvent, item: Item) {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    this.contextMenu.menuData = { 'item': item };
    this.contextMenu.openMenu();
  }

  onContextMenuAction1(item: Item) {
    this.teamA.push(item.name);
    // alert(`Selcted ${item.name} to Team A`);
  }

  onContextMenuAction2(item: Item) {
      this.teamB.push(item.name);
    // alert(`Selcted ${item.name} to Team B`);
  }
}

export interface Item {
  id: number;
  name: string;
}