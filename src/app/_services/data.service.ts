import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
@Injectable({
    providedIn: 'root'
})
export class DataService {
    constructor(private http: HttpClient) { }

    getNewsServiceCall() {
        return this.http.get('https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=045919187fe9401f8944202cad76cfd1'
);
}
}