
// Parser to convert API response to simplify as needed.
export function pasreResponse(data: any) {
    if(data && data.articles) {
      let articles = data.articles.map(item => {
        let date = new Date(item.publishedAt);
        item.publishedAt = date;
        return item;
      });
    return articles;
  }return null;
  }