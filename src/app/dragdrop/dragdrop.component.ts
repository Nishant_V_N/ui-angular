import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
@Component({
  selector: 'app-dragdrop',
  templateUrl: './dragdrop.component.html',
  styleUrls: ['./dragdrop.component.css']
})
export class DragdropComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
 todo = [
    'Get to work',
    'Pick up groceries',
    'Go home',
    'Fall asleep'
];

like = [
    'Get up',
    'Brush teeth',
    'Take a shower',
    'Check e-mail',
    'Walk dog'
];
dislike = [
    'dislike1',
    'dislike2',
    'dislike3',
    'dislike4',
    'dislike5'
];
notinterested = [
    'notinterested1',
    'notinterested2',
    'notinterested3',
    'notinterested4',
    'notinterested5'
];
todoObj: any;
todoItem: string;
drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
        transferArrayItem(event.previousContainer.data,
            event.container.data,
            event.previousIndex,
            event.currentIndex);
    }
}
addItem(event) {
    if( this.todoItem &&  this.todoItem.trim().length) {
      this.todo.push(this.todoItem);
      this.todoItem = '';
    }
  }
  deleteItem(event) {
    this.todo.splice(event, 1);
  }
  

}
